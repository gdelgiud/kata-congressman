# Kata Rx -  Honorable Cámara de Diputados

Se desea modelar el proceso de votación de una ley realizado por la Honorable Cámara de Diputados.
El objetivo de la kata es conocer distintos operadores reactivosos.

## Step 1
- El diputado debe poder votar una ley, indicando para la misma si su voto es a favor, en contra o una abstención
- El criterio usado para votar está exclusivamente relacionado con la longitud de la ley. Si la misma es corta entonces se aprueba, si la misma es larga se rechaza, y si la misma no tiene nada escrito se abstiene

**Requerimientos**
- El código debe ser 100% reactivoso

### Step 1.1
- Si la forma de testear fue utilizando algun tipo de lógica bloqueante, reemplazarla por las clases de testing unitario especialmente creadas para Rx (y usar estas para todos los tests posteriores)
- Deben estar testeados todas las variantes descriptas en el paso previo


## Step 2
- La Honorable Cámara de Diputados, la cual está formada por un conjunto de diputados, debe poder indicar si una ley es aprobada o rechazada.
- El criterio de aprobación de una ley es “mayoría simple”

### Step 2.1
- Si para la solución no se utilizó el operador `flatmap()`, realizar otra implementación que si lo utilice. En caso de sí haber utilizado flatmap, realizar otra implementación que tenga la misma función que dicho operador pero sin utilizarlo (combinando otros operadores existentes)

## Step 3
- A los diputados ahora les toma un tiempo leer la ley antes de emitir si voto. Tardan 100 ms por cada caracter que tenga la ley

### Step 3.1
- Si para testear se usó un sleep feo en algun lugar, refactorizar el código y/o los tests para evitarlo. La duración del test no debería verse afectada por el tiempo de retardo de los diputados, aunque igualmente el test debería garantizar que los mismos efectivamente se toman el tiempo requerido para votar.


## Step 4
- Existe un nuevo tipo de diputado “Boicoteador”. Cuando le piden votar, se pone a gritar “Acá no se vota!”, lo cual termina cancelando todo el proceso de votación
- Para la cámara de diputados, mas allá del escándalo generado, cuando esto sucede la ley simplemente se considera rechazada

### Step 4.1
- Si se arrojó alguna excepción para modelar el flujo, refactorizar el código para no hacerlo y a cambio usar algún operador de rx que permita indicar un error.


## Step 5
- Se agrega otro tipo de diputado, el "Cometeable".
- Este diputado, en caso de recibir una “cometa”, aprueba la ley. En caso negativo, se comporta como un diputado común.
- Aún cuando el diputado tome el camino “cometa-oriented”, para disimular siempre se comporta inicialmente como un diputado común (se toma el tiempo necesario, etc)

## Step 6
- Todos los diputados deben tener un identificador. Cada vez que un diputado vota debe loguear por consola su identificación, su tipo, y el voto decidido.
- La camara de diputados debería loguear el comienzo y fin de cada votación , indicando la ley que se vota

**Requerimiento**
- Todos los logueos deberían realizarse de forma desacoplada de la lógica de negocio.

## Step 7
- Se agrega el diputado "Ansioso". Este diputado se comporta como un diputado normal, pero si para decidir su voto toma mas de 300 ms, interrumpe su lectura y rechaza la ley sin mayor análisis
- Se agrega el diputado que usa anteojos, el cual necesita ponerse los anteojos antes de decidir como vota (donde su criterio es el de un diputado normal)

